package uk.co.nickthecoder.downy.scarea

import org.antlr.v4.runtime.CharStream
import org.antlr.v4.runtime.CharStreams
import org.antlr.v4.runtime.CommonTokenStream
import org.antlr.v4.runtime.Token
import org.antlr.v4.runtime.tree.ErrorNode
import org.antlr.v4.runtime.tree.ParseTreeWalker
import org.antlr.v4.runtime.tree.TerminalNode
import uk.co.nickthecoder.feather.parser.FeatherLexer
import uk.co.nickthecoder.feather.parser.FeatherParser
import uk.co.nickthecoder.feather.parser.FeatherParserBaseListener
import uk.co.nickthecoder.scarea.HighlightRange
import uk.co.nickthecoder.scarea.syntax.AntlrSyntaxHighlighter

class FeatherSyntaxHighlighter : AntlrSyntaxHighlighter() {

    companion object {
        val instance = FeatherSyntaxHighlighter()
    }

    override fun createWorker(): Worker {
        return FeatherWorker()
    }

    private inner class FeatherWorker : Worker() {

        override fun createRanges(text: String): List<HighlightRange> {

            val lexer = CommentedFeatherLexer(CharStreams.fromString(text))
            lexer.removeErrorListeners()
            val tokens = CommonTokenStream(lexer)
            val parser = FeatherParser(tokens)
            parser.removeErrorListeners()

            val listener = Listener()
            val tree = parser.featherFile()
            ParseTreeWalker.DEFAULT.walk(listener, tree)

            return ranges
        }

        private inner class CommentedFeatherLexer(source: CharStream) : FeatherLexer(source) {

            override fun nextToken(): Token {
                val token = super.nextToken()
                if (token.channel == 1) {
                    highlight(token, "comment")
                }

                return token
            }
        }

        private inner class Listener : FeatherParserBaseListener() {


            override fun visitErrorNode(node: ErrorNode) {
                highlight(node.symbol, "error")
            }

            override fun exitPackageHeader(ctx: FeatherParser.PackageHeaderContext) {
                highlight(ctx.PACKAGE())
            }

            override fun exitImportHeader(ctx: FeatherParser.ImportHeaderContext) {
                highlight(ctx.IMPORT())
                highlight(ctx.importAlias()?.AS())
            }

            override fun exitIncludeHeader(ctx: FeatherParser.IncludeHeaderContext) {
                highlight(ctx.INCLUDE())
            }

            override fun exitClassModifiersAndName(ctx: FeatherParser.ClassModifiersAndNameContext) {
                highlight(ctx.CLASS())
                highlight(ctx.INTERFACE())
                highlight(ctx.ABSTRACT())
            }

            override fun exitConstructorParameter(ctx: FeatherParser.ConstructorParameterContext) {
                highlight(ctx.VAL())
                highlight(ctx.VAR())
            }

            override fun exitClassBody(ctx: FeatherParser.ClassBodyContext) {
                highlightPair(ctx.LCURL(), ctx.RCURL())
            }

            override fun exitFieldDeclarationDescription(ctx: FeatherParser.FieldDeclarationDescriptionContext) {
                highlight(ctx.STATIC())
                highlight(ctx.VAL())
                highlight(ctx.VAR())
            }

            override fun exitMethodDescription(ctx: FeatherParser.MethodDescriptionContext) {
                highlight(ctx.STATIC())
                highlight(ctx.ABSTRACT())
                highlight(ctx.OVERRIDE())
                highlight(ctx.FUN())
            }

            override fun exitMethodValueParameters(ctx: FeatherParser.MethodValueParametersContext) {
                highlightPair(ctx.LPAREN(), ctx.RPAREN())
            }

            override fun exitBlock(ctx: FeatherParser.BlockContext) {
                highlightPair(ctx.LCURL(), ctx.RCURL())
            }

            override fun exitLocalVariableDeclaration(ctx: FeatherParser.LocalVariableDeclarationContext) {
                highlight(ctx.VAL())
                highlight(ctx.VAR())
            }

            override fun exitThrowStatement(ctx: FeatherParser.ThrowStatementContext) {
                highlight(ctx.THROW())
            }

            override fun exitReturnStatement(ctx: FeatherParser.ReturnStatementContext) {
                highlight(ctx.RETURN())
            }

            override fun exitContinueStatement(ctx: FeatherParser.ContinueStatementContext) {
                highlight(ctx.CONTINUE())
            }

            override fun exitBreakStatement(ctx: FeatherParser.BreakStatementContext) {
                highlight(ctx.BREAK())
            }

            override fun exitNamedInfix(ctx: FeatherParser.NamedInfixContext) {
                highlight(ctx.IS())
                highlight(ctx.NOT_IS())
            }

            override fun exitArrayAccess(ctx: FeatherParser.ArrayAccessContext) {
                highlightPair(ctx.LSQUARE(), ctx.RSQUARE())
            }

            override fun exitValueArguments(ctx: FeatherParser.ValueArgumentsContext) {
                highlightPair(ctx.LPAREN(), ctx.RPAREN())
            }

            override fun exitTypeArguments(ctx: FeatherParser.TypeArgumentsContext) {
                highlightPair(ctx.LANGLE(), ctx.RANGLE())
            }

            override fun exitThisExpression(ctx: FeatherParser.ThisExpressionContext) {
                highlight(ctx.THIS())
            }

            override fun exitWithDescription(ctx: FeatherParser.WithDescriptionContext) {
                highlight(ctx.WITH())
                highlightPair(ctx.LPAREN(), ctx.RPAREN())
            }

            override fun exitReceiverDescription(ctx: FeatherParser.ReceiverDescriptionContext) {
                highlight(ctx.APPLY())
            }

            override fun exitParenthesizedExpression(ctx: FeatherParser.ParenthesizedExpressionContext) {
                highlightPair(ctx.LPAREN(), ctx.RPAREN())
            }

            override fun exitIfExpression(ctx: FeatherParser.IfExpressionContext) {
                if (ctx.ELSE() == null) {
                    highlight(ctx.IF() as TerminalNode?) // Hmm, why  is the compiler moaning when the "as" is removed???
                } else {
                    highlightPair(ctx.IF(), ctx.ELSE(), "keyword")
                }
            }

            override fun exitForSetup(ctx: FeatherParser.ForSetupContext) {
                highlight(ctx.FOR())
                highlightPair(ctx.LPAREN(), ctx.RPAREN())
            }

            override fun exitWhileExpression(ctx: FeatherParser.WhileExpressionContext) {
                highlight(ctx.WHILE())
                highlightPair(ctx.LPAREN(), ctx.RPAREN())
            }

            override fun exitDoWhileExpression(ctx: FeatherParser.DoWhileExpressionContext) {
                highlightPair(ctx.DO(), ctx.WHILE(), "keyword")
                highlightPair(ctx.LPAREN(), ctx.RPAREN())
            }

            override fun exitTryExpression(ctx: FeatherParser.TryExpressionContext) {
                highlight(ctx.TRY())
            }

            override fun exitCatchBlock(ctx: FeatherParser.CatchBlockContext) {
                highlight(ctx.CATCH())
                highlightPair(ctx.LPAREN(), ctx.RPAREN())
            }

            override fun exitFinallyBlock(ctx: FeatherParser.FinallyBlockContext) {
                highlight(ctx.FINALLY())
            }

            override fun exitNullLiteral(ctx: FeatherParser.NullLiteralContext) {
                highlight(ctx)
            }

            override fun exitBooleanLiteral(ctx: FeatherParser.BooleanLiteralContext?) {
                highlight(ctx)
            }

            override fun exitCharacterLiteral(ctx: FeatherParser.CharacterLiteralContext) {
                highlight(ctx, "string")
            }

            override fun exitDoubleLiteral(ctx: FeatherParser.DoubleLiteralContext?) {
                highlight(ctx, "number")
            }

            override fun exitFloatLiteral(ctx: FeatherParser.FloatLiteralContext?) {
                highlight(ctx, "number")
            }

            override fun exitByteLiteral(ctx: FeatherParser.ByteLiteralContext?) {
                highlight(ctx, "number")
            }

            override fun exitShortLiteral(ctx: FeatherParser.ShortLiteralContext?) {
                highlight(ctx, "number")
            }

            override fun exitIntLiteral(ctx: FeatherParser.IntLiteralContext?) {
                highlight(ctx, "number")
            }

            override fun exitLongLiteral(ctx: FeatherParser.LongLiteralContext?) {
                highlight(ctx, "number")
            }

            override fun exitLineStringLiteral(ctx: FeatherParser.LineStringLiteralContext) {
                highlightPair(ctx.QUOTE_OPEN(), ctx.QUOTE_CLOSE())
            }

            override fun exitMultiLineStringLiteral(ctx: FeatherParser.MultiLineStringLiteralContext) {
                highlightPair(ctx.TRIPLE_QUOTE_OPEN(), ctx.TRIPLE_QUOTE_CLOSE())
            }

            override fun exitLineStringContent(ctx: FeatherParser.LineStringContentContext) {
                highlight(ctx, "string")
            }

            override fun exitLineStringExpression(ctx: FeatherParser.LineStringExpressionContext) {
                highlightPair(ctx.LineStrExprStart(), ctx.RCURL())
            }

            override fun exitMultiLineStringContent(ctx: FeatherParser.MultiLineStringContentContext) {
                highlight(ctx, "string")
            }

            override fun exitMultiLineStringExpression(ctx: FeatherParser.MultiLineStringExpressionContext) {
                highlightPair(ctx.MultiLineStrExprStart(), ctx.RCURL())
            }

            override fun exitCommandLiteral(ctx: FeatherParser.CommandLiteralContext) {
                highlightPair(ctx.COMMAND_OPEN(), ctx.COMMAND_CLOSE())
            }

            override fun exitCommandContent(ctx: FeatherParser.CommandContentContext) {
                highlight(ctx, "string")
            }

            override fun exitCommandExpression(ctx: FeatherParser.CommandExpressionContext) {
                highlightPair(ctx.CommandExprStart(), ctx.RCURL())
            }

            override fun exitGenericSpec(ctx: FeatherParser.GenericSpecContext) {
                highlightPair(ctx.LANGLE(), ctx.RANGLE())
            }

            override fun exitAnnotation(ctx: FeatherParser.AnnotationContext?) {
                highlight(ctx, "comment")
            }
        }
    }

}
