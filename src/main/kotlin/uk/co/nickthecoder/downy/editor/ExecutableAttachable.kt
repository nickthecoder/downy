package uk.co.nickthecoder.downy.editor

import uk.co.nickthecoder.fxessentials.ActionGroup
import java.io.File
import java.util.concurrent.TimeUnit

/**
 * Adds extra buttons and menu items to allow a tab to be run.
 * i.e. the contents are an executable script, with a suitable #!
 * at the start.
 */
class ExecutableAttachable(val gui: GUI) {

    private val actionGroup = ActionGroup().apply {
        action(FILE_RUN) { run() }
        action(FILE_RUN_ARGS) { runArgs() }
    }

    private val runButton = actionGroup.button(FILE_RUN)
    private val runArgsButton = actionGroup.button(FILE_RUN_ARGS)
    private val runMenuItem = actionGroup.menuItem(FILE_RUN)
    private val runArgsMenuItem = actionGroup.menuItem(FILE_RUN_ARGS)

    init {
        gui.addControl(runButton)
        gui.addControl(runArgsButton)
        gui.addMenuItem("file", runMenuItem)
        gui.addMenuItem("file", runArgsMenuItem)
    }

    fun detach() {
        gui.removeControl(runButton)
        gui.removeControl(runArgsButton)
        gui.removeMenuItem("file", runMenuItem)
        gui.removeMenuItem("file", runArgsMenuItem)
    }

    private fun postSave(action: (File) -> Unit) {
        gui.currentEditorTab()?.let { tab ->
            tab.save()
            tab.file?.let { file ->
                ProcessBuilder().apply {
                    command("chmod", "+x", file.path)
                }.start().waitFor(1, TimeUnit.SECONDS)
                action(file)
            }
        }
    }

    private fun run() {
        postSave { file ->
            runCommand(gui, listOf(file.path))
        }
    }

    private fun runArgs() {
        postSave { file ->
            ArgsTask(gui, file).prompt()
        }
    }

}
