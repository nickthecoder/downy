package uk.co.nickthecoder.downy.editor

import javafx.beans.binding.Bindings
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.collections.ListChangeListener
import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.control.*
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyEvent
import javafx.scene.layout.BorderPane
import javafx.scene.layout.VBox
import javafx.stage.FileChooser
import javafx.stage.Stage
import uk.co.nickthecoder.fxessentials.ActionGroup
import uk.co.nickthecoder.fxessentials.RecentFiles
import uk.co.nickthecoder.fxessentials.hideTabs
import uk.co.nickthecoder.harbourfx.Dockable
import uk.co.nickthecoder.harbourfx.DockableFactory
import uk.co.nickthecoder.harbourfx.Harbour
import uk.co.nickthecoder.scarea.Scarea
import uk.co.nickthecoder.scarea.extra.ui.FindBar
import uk.co.nickthecoder.scarea.extra.ui.GotoDialog
import uk.co.nickthecoder.scarea.extra.ui.ReplaceBar
import uk.co.nickthecoder.scarea.extra.ui.ScareaMatcher
import uk.co.nickthecoder.scarea.extra.util.RemoveHiddenChildren
import uk.co.nickthecoder.scarea.extra.util.onSceneAvailable
import java.io.File

class GUI() : BorderPane(), DockableFactory {

    private val toolBar = ToolBar()

    private val fileMenu = Menu("_File")

    private val editMenu = Menu("_Edit")

    private val viewMenu = Menu("_View")

    private val fileTypesMenu: Menu

    private val menuBar = MenuBar(fileMenu, editMenu, viewMenu)

    private val tabPane = TabPane()

    private val splitPane = SplitPane().apply {
        items.add(tabPane)
    }

    val harbour = Harbour(splitPane, this)


    /**
     * The non-gui part controlling the Find/Replace bars.
     */
    private val matcher = ScareaMatcher(Scarea())

    /**
     * A tool bar, which appears below the tabPane (inside findAndReplaceToolBars)
     */
    private val findBar = FindBar(matcher)

    /**
     * A tool bar, which appears below the findBar (inside findAndReplaceToolBars)
     */
    private val replaceBar = ReplaceBar(matcher)

    /**
     * At the bottom of the scene. Contains findBar and replaceBar.
     */
    private val findAndReplaceToolBars = VBox().apply {
        // Automatically removes children when they are made invisible.
        // Then replaces them if they are made visible again.
        // Without this, the findAndReplaceToolBars VBox would take up space even when its children were hidden.
        RemoveHiddenChildren(children)
        children.addAll(findBar.toolBar, replaceBar.toolBar)
    }


    /**
     * Used where the DOCUMENT_OPEN menu button and the "recent" menu are created.
     * It is important to keep a reference to the handler the [RecentFiles] uses to open documents,
     * because it stores this using a weak reference. It does this to protect against memory leaks.
     * Note, if you prefer, we could have defined it like this :
     *
     *      openFileHandler = { file: File -> openFile(file) }
     *
     */
    private val openFileHandler = ::openFile


    private val currentTabIsEditor = SimpleBooleanProperty(false)
    private val currentTabIsNotEditor = currentTabIsEditor.not()
    private val currentFileTypeProperty = SimpleObjectProperty<FileType>(UnknownFileType.instance).apply {
        addListener { _, oldValue, newValue -> fileTypeChanged(oldValue, newValue) }
    }
    private var fileTypeAttachInfo: Any? = null


    private val actionGroup = ActionGroup().apply {
        action(DOCUMENT_NEW) { newTab() }
        action(DOCUMENT_SAVE, disabled = currentTabIsNotEditor) { currentEditorTab()?.save() }
        action(DOCUMENT_SAVE_AS, disabled = currentTabIsNotEditor) { currentEditorTab()?.saveAs() }
        action(DOCUMENT_OPEN) { open() }
        action(TAB_CLOSE, disabled = noTabs) { tabPane.selectionModel.selectedItem?.let { tabPane.tabs.remove(it) } }

        action(EDIT_FIND, disabled = currentTabIsNotEditor) {
            matcher.inUse = true
            findBar.requestFocus()
        }
        action(EDIT_REPLACE, disabled = currentTabIsNotEditor) {
            val wasInUse = matcher.inUse
            replaceBar.toolBar.isVisible = true
            if (wasInUse) {
                replaceBar.requestFocus()
            } else {
                findBar.requestFocus()
            }
        }
        action(EDIT_GOTO, disabled = currentTabIsNotEditor) {
            GotoDialog(matcher.scarea).show()
        }
        action(TABS_TO_SPACES, disabled = currentTabIsNotEditor) {
            currentEditorTab()?.tabsToSpaces()
        }
        action(EDIT_SETTINGS) { Settings.instance.prompt() }

        action(VIEW_LOG_TOGGLE) { harbour.toggle(LogDockable.id) }

        action(FILE_TYPES, disabled = currentTabIsNotEditor) {}
    }

    /**
     * The save menu item and the save button will be disabled when there are no tabs open.
     * Notice how we only use [noTabs] ONCE when added to the action group, and menu item AND the button
     * both become disabled when there are no tabs open.
     */
    private val noTabs = Bindings.isNull(tabPane.selectionModel.selectedItemProperty())

    init {

        top = VBox().apply { children.addAll(menuBar, toolBar) }
        center = harbour
        bottom = findAndReplaceToolBars

        with(tabPane) {

            selectionModel.selectedItemProperty().addListener { _, _, newTab -> tabChanged(newTab) }

            // Hides the header area of the tab pane when there is only one tab.
            tabs.addListener(ListChangeListener { tabPane.hideTabs = tabPane.tabs.size < 2 })

        }

        matcher.inUse = false // Hide the find and replace bars
        prefWidth = 900.0
        prefHeight = 700.0

        addEventFilter(KeyEvent.KEY_PRESSED) { event ->
            if (event.code == KeyCode.ESCAPE) {
                matcher.inUse = false
                currentEditorTab()?.scarea?.requestFocus()
            }
        }
        with(actionGroup) {
            fileMenu.items.addAll(
                menuItem(DOCUMENT_NEW),
                menuItem(DOCUMENT_SAVE),
                menuItem(DOCUMENT_SAVE_AS),
                menuItem(DOCUMENT_OPEN),
                Menu("Recent").apply { DownyApp.recentFiles.addMenu(this, openFileHandler) }
            )

            editMenu.items.addAll(
                menuItem(EDIT_FIND),
                menuItem(EDIT_REPLACE),
                menuItem(EDIT_GOTO),
                menuItem(TABS_TO_SPACES),
                SeparatorMenuItem(),
                menuItem(EDIT_SETTINGS)
            )

            fileTypesMenu = menu(FILE_TYPES)
            fileTypes.forEach { fileType ->
                fileTypesMenu.items.add(CheckMenuItem(fileType.label).apply {
                    onAction = EventHandler { changeFileType(fileType) }
                })
            }
            viewMenu.items.addAll(
                fileTypesMenu,
                menuItem(VIEW_LOG_TOGGLE)
            )

            toolBar.items.addAll(
                button(DOCUMENT_NEW),
                actionGroup.splitMenuButton(DOCUMENT_OPEN)
                    .apply { DownyApp.recentFiles.addMenu(this, openFileHandler) },
                button(DOCUMENT_SAVE),
            )
        }

    }

    fun currentEditorTab(): EditorTab? = tabPane.selectionModel.selectedItem as? EditorTab

    private fun tabChanged(newTab: Tab?) {
        currentTabIsEditor.value = newTab is EditorTab

        currentFileTypeProperty.unbind()
        val currentTab = currentEditorTab()
        if (currentTab != null) {
            currentFileTypeProperty.bind(currentTab.fileTypeProperty())
        }
        for (item in fileTypesMenu.items) {
            if (item is CheckMenuItem) {
                item.isSelected = currentFileTypeProperty.value?.label == item.text
            }
        }
        (scene?.window as Stage?)?.title = "${DownyApp.DEFAULT_TITLE} : ${newTab?.text ?: ""}"
    }


    private fun fileTypeChanged(oldValue: FileType, newValue: FileType) {
        if (oldValue !== newValue) {
            oldValue.detach(this, fileTypeAttachInfo)
            fileTypeAttachInfo = newValue.attach(this)
        }
    }

    private fun changeFileType(fileType: FileType) {
        currentEditorTab()?.fileType = fileType
        fileTypesMenu.items.forEach { item ->
            if (item is CheckMenuItem) {
                item.isSelected = item.text === fileType.label
            }
        }
    }

    private fun newTab() {
        val tab = EditorTab(null)
        tabPane.tabs.add(tab)
        tabPane.selectionModel.select(tab)
        tab.scarea.onSceneAvailable { tab.scarea.requestFocus() }
    }

    private fun open() {
        val currentTab = tabPane.selectionModel.selectedItem as? EditorTab
        val file = FileChooser().apply {
            title = "Open Script"
            extensionFilters.addAll(
                FileChooser.ExtensionFilter("All Files", "*"),
                FileChooser.ExtensionFilter("Text Files", "*.txt"),
                FileChooser.ExtensionFilter("Feather Script", "*.feather")
            )
            currentTab?.file?.let {
                initialDirectory = it.parentFile
            }
        }.showOpenDialog(scene.window)

        file?.let { openFile(it) }
    }

    fun openFile(file: File) {
        val tab = EditorTab(file)
        tabPane.tabs.add(tab)
        tabPane.selectionModel.select(tabPane.tabs.size - 1)
        DownyApp.recentFiles.add(file)
        tab.scarea.onSceneAvailable { tab.scarea.requestFocus() }
    }


    fun createDefaultDocks() {
        harbour.bottom.mainHalf.dockables.add(harbour.factory.createDockable(LogDockable.id)!!)
    }

    /**
     * allows items to be added to the [ToolBar] without exposing the toolbar control.
     */
    fun addControl(control: Node) {
        toolBar.items.add(control)
    }

    fun removeControl(control: Node) {
        toolBar.items.remove(control)
    }

    private fun menu(menuName: String) = when (menuName) {
        "file" -> fileMenu
        "edit" -> editMenu
        "view" -> viewMenu
        else -> fileMenu
    }

    /**
     * allows items to be added to the [MenuBar] without exposing the menu bar control.
     */
    fun addMenuItem(menuName: String, item: MenuItem) {
        menu(menuName).items.add(item)
    }

    fun removeMenuItem(menuName: String, item: MenuItem) {
        menu(menuName).items.remove(item)
    }

    override fun createDockable(dockableId: String): Dockable? {
        return when (dockableId) {
            LogDockable.id -> LogDockable()

            else -> null
        }
    }

}
