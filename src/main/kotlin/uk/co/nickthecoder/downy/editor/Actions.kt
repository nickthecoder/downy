package uk.co.nickthecoder.downy.editor

import javafx.scene.input.KeyCode
import uk.co.nickthecoder.fxessentials.AllActions


val actions = AllActions(DownyApp::class.java)

val DOCUMENT_OPEN = actions.create("document-open", "Open", KeyCode.O, control = true)
val DOCUMENT_SAVE = actions.create("document-save", "Save", KeyCode.S, control = true, shift = false)
val DOCUMENT_SAVE_AS = actions.create("document-save-as", "Save As", KeyCode.S, control = true, shift = true)
val DOCUMENT_NEW = actions.create("document-new", "New", KeyCode.N, control = true)

val TAB_CLOSE = actions.create("tab-close", "Close Tab", KeyCode.W, control = true)

val FILE_TYPES = actions.create("filetypes", "File Type")

val EDIT_FIND = actions.create("edit-find", "Find", KeyCode.F, control = true)
val EDIT_REPLACE = actions.create("edit-replace", "Replace", KeyCode.R, control = true)
val EDIT_GOTO = actions.create("edit-goto", "Go to", KeyCode.G, control = true)

val EDIT_SETTINGS = actions.create("edit-settings", "Settings")
val TABS_TO_SPACES = actions.create("convert-tabs-to-spaces", "Tabs -> Spaces")

val VIEW_LOG_TOGGLE = actions.create("view-log", "Log", KeyCode.DIGIT2, alt = true, shift = false)

val FILE_RUN = actions.create("file-run", "Run", KeyCode.R, control = true)
val FILE_RUN_ARGS = actions.create("file-run-args", "Run ...", KeyCode.R, control = true, shift=true)
