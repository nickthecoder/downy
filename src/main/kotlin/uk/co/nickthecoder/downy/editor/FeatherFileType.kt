package uk.co.nickthecoder.downy.editor

import javafx.beans.InvalidationListener
import javafx.stage.FileChooser
import uk.co.nickthecoder.downy.scarea.FeatherSyntaxHighlighter
import uk.co.nickthecoder.scarea.Scarea


class FeatherFileType private constructor() : FileType {

    override val label: String
        get() = "Feather Script"

    override fun attach(scarea: Scarea) = FeatherSyntaxHighlighter.instance.attach(scarea)

    override fun detach(scarea: Scarea, attachInfo: Any?) {
        if (attachInfo is InvalidationListener) {
            FeatherSyntaxHighlighter.instance.detach(scarea, attachInfo)
        }
    }

    override fun saveAsDialog() = FileChooser().apply {
        title = "Save Feather Script"
        extensionFilters.addAll(
            FileChooser.ExtensionFilter("Feather Scripts", "*.feather"),
            FileChooser.ExtensionFilter("All Files", "*")
        )
    }


    override fun attach(gui: GUI) = ExecutableAttachable(gui)

    override fun detach(gui: GUI, attachInfo: Any?) {
        (attachInfo as? ExecutableAttachable)?.detach()
    }


    override fun score(tab: EditorTab): Int {
        if (tab.file?.extension == "feather") return 10
        if (tab.scarea.document.paragraphs.isNotEmpty()) {
            val line1 = tab.scarea.document.paragraphs[0]
            if (line1.startsWith("#!") && line1.endsWith("feathers")) {
                return 8
            }
        }
        return 0
    }


    companion object {
        val instance = FeatherFileType()
    }
}
