package uk.co.nickthecoder.downy.editor

import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.event.EventHandler
import javafx.scene.control.Label
import javafx.scene.control.Tab
import javafx.scene.control.Tooltip
import javafx.scene.input.KeyCode
import javafx.scene.layout.BorderPane
import uk.co.nickthecoder.feather.CompilationFailed
import uk.co.nickthecoder.feather.FeatherException
import uk.co.nickthecoder.fxessentials.showSaveDialogWithExtension
import uk.co.nickthecoder.scarea.FillStyleHighlight
import uk.co.nickthecoder.scarea.HighlightRange
import uk.co.nickthecoder.scarea.Scarea
import uk.co.nickthecoder.scarea.ScareaPosition
import uk.co.nickthecoder.scarea.syntax.SyntaxHighlighter
import java.io.File
import java.io.PrintWriter
import java.io.StringWriter

class EditorTab(
    var file: File?

) : Tab() {

    val scarea = Scarea()

    /**
     * The file type - this determines how the text is highlighted, and
     * also some file types can add extra options to the main window.
     */
    fun fileTypeProperty(): ObjectProperty<FileType> = fileTypeProperty
    private val fileTypeProperty = SimpleObjectProperty<FileType>(UnknownFileType.instance).apply {
        addListener { _, oldValue, newValue ->
            oldValue.detach(scarea, fileTypeAttachInfo)
            fileTypeAttachInfo = newValue.attach(scarea)
        }
    }
    var fileType: FileType
        get() = fileTypeProperty.value
        set(v) {
            fileTypeProperty.value = v
        }

    init {
        graphic = if (file == null) Label("*") else fileType.graphic()
        text = if (file == null) "Untitled" else file!!.name
        guessFileType()
    }

    /**
     * The return value from [FileType.attach], which is passed back into [FileType.detach]
     */
    private var fileTypeAttachInfo: Any? = null

    protected val errorMessage = Label()

    protected val borderPane = BorderPane().apply {
        center = scarea
    }


    private var errorHighlightRange: HighlightRange? = null
    private val errorHighlight = FillStyleHighlight("-fx-fill: black;", "-fx-fill: #ffcccc;")

    init {

        scarea.document.indentSizeProperty().bind(Settings.instance.tabSizeP.integerValueProperty())
        scarea.onKeyPressed = EventHandler { event ->
            if (event.code == KeyCode.ESCAPE) {
                // Note, the event is not consumed, so Scarea will process the key too.
            }
        }

        file?.let {
            scarea.text = it.readText()
            if (!it.canWrite()) {
                scarea.editable = false
            }
        }
        content = borderPane

        guessFileType()
    }


    fun handleError(e: Exception) {
        errorMessage.text = e.toString()
        if (errorMessage.text.length > 200) errorMessage.text = errorMessage.text.substring(0, 200) + "..."
        borderPane.bottom = errorMessage

        val strWriter = StringWriter()
        e.printStackTrace(PrintWriter(strWriter))
        errorMessage.tooltip = Tooltip(strWriter.toString())

        if (e is FeatherException) {
            highlightError(e)
            Log.reportError(e)

        } else if (e is CompilationFailed) {
            e.errors.firstOrNull()?.let {
                highlightError(it)
                Log.reportError(it)
            }
        } else if (e is InterruptedException) {
            // Do nothing
            Unit
        } else {
            Log.reportError(e)
        }
    }

    fun clearErrors() {
        borderPane.bottom = null
        errorHighlightRange?.let { scarea.document.removeHighlightRange(it) }
        val errors = scarea.document.ranges.filter { it.highlight == SyntaxHighlighter.DEFAULT_ERROR_HIGHLIGHT }
        scarea.document.removeHighlightRanges(errors)
    }


    private fun highlightError(e: FeatherException) {
        scarea.caretPosition = ScareaPosition(e.pos.line, e.pos.column)
        errorHighlightRange = HighlightRange(
            ScareaPosition(e.pos.line, 0),
            ScareaPosition(e.pos.line + 1, 0),
            errorHighlight
        )
        scarea.document.addHighlightRange(errorHighlightRange!!)
        scarea.ensureCaretVisible()
        scarea.requestFocus()
    }

    fun save() {
        scarea.requestFocus()
        if (file == null) {
            saveAs()
        } else {
            file!!.writeText(scarea.text)
        }
        graphic = fileType.graphic()
    }

    fun saveAs() {

        val saveFile = fileType.saveAsDialog().showSaveDialogWithExtension(tabPane.scene.window)
        if (saveFile != null) {
            guessFileType()
            file = saveFile
            save()
            text = saveFile.name // Change the tab's text.
            DownyApp.recentFiles.add(saveFile)
        }
    }

    /**
     * Use the #! line to guess the file type if [fileType] is currently [UnknownFileType].
     */
    private fun guessFileType() {
        if (fileType === UnknownFileType.instance) {
            var bestScore = 0
            var bestFileType: FileType = UnknownFileType.instance
            for (fileType in fileTypes) {
                val score = fileType.score(this)
                if (score > bestScore) {
                    bestScore = score
                    bestFileType = fileType
                }
            }
            fileType = bestFileType
        }
    }

    fun tabsToSpaces() {
        scarea.text = scarea.text.replace("\t", " ".repeat(scarea.document.indentSize))
    }

}
