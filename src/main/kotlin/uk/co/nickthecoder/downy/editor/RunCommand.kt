package uk.co.nickthecoder.downy.editor

import javafx.application.Platform
import uk.co.nickthecoder.paratask.AbstractParameterizedTask
import uk.co.nickthecoder.paratask.parameters.InformationParameter
import uk.co.nickthecoder.paratask.parameters.StringParameter
import java.io.File
import java.io.InputStreamReader


class ArgsTask(
    private val gui: GUI,
    private val command: File

) : AbstractParameterizedTask("run") {

    private val commandP = StringParameter("command", command.path).apply {
        enabled = false
    }

    private val argsP = StringParameter("arguments").apply {
        required = false
        rows = 10
    }
    private val infoP = InformationParameter("info", "Each argument must be on its own line")

    init {
        parameters.addAll(commandP, argsP, infoP)
    }

    override fun run(): Process {
        val command = mutableListOf(command.path).apply { addAll(argsP.value.split("\n")) }
        return runCommand(gui, command)
    }
}


fun runCommand(gui: GUI, command: List<String>): Process {

    if (Settings.instance.clearLog == true) {
        (gui.harbour.findDockable(LogDockable.id) as? LogDockable)?.clear()
    }

    var openedLog = false
    fun openLog() {
        if (!openedLog) {
            openedLog = true
            gui.harbour.show(LogDockable.id)
        }
    }

    val process = ProcessBuilder().apply {
        command(command)
    }.start()

    // Three threads... Copies stdout to the Log, Copies stderr to the Log
    // and checks the exit status, and dings or errors.
    Thread {
        val out = InputStreamReader(process.inputStream)
        out.forEachLine { line ->
            Platform.runLater {
                Log.println(line)
                openLog()
            }
        }
    }.start()
    Thread {
        val err = InputStreamReader(process.errorStream)
        err.forEachLine { line ->
            Platform.runLater {
                Log.println(line)
                openLog()
            }
        }
    }.start()
    Thread {
        val exit = process.waitFor()
        Platform.runLater {
            if (exit != 0) {
                Log.println("Exit status = $exit")
                openLog()
                DownyApp.playError()
            } else {
                DownyApp.playDing()
            }
        }
    }.start()

    return process
}
