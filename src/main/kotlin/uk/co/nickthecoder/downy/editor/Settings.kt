package uk.co.nickthecoder.downy.editor

import uk.co.nickthecoder.paratask.AbstractParameterizedTask
import uk.co.nickthecoder.paratask.gui.TaskPrompter
import uk.co.nickthecoder.paratask.parameters.BooleanParameter
import uk.co.nickthecoder.paratask.parameters.DoubleParameter
import uk.co.nickthecoder.paratask.parameters.IntegerParameter
import uk.co.nickthecoder.paratask.util.loadFromPreferences
import uk.co.nickthecoder.paratask.util.saveToPreferences
import java.util.prefs.Preferences

internal open class Settings private constructor() : AbstractParameterizedTask("settings") {

    private val preferences = Preferences.userNodeForPackage(Settings::class.java)

    private val windowXP = DoubleParameter("windowX", 800.0).apply { visible = false }
    private val windowYP = DoubleParameter("windowY", 600.0).apply { visible = false }
    private val windowWidthP = DoubleParameter("windowWidth", 800.0).apply { visible = false }
    private val windowHeightP = DoubleParameter("windowHeight", 600.0).apply { visible = false }
    private val windowIsMaximisedP = BooleanParameter("windowIsMaximised", false).apply { visible = false }

    val tabSizeP = IntegerParameter("tabSize", 4)

    val clearLogP = BooleanParameter("clearLog", true).apply {
        label = "Clear the Log"
        suffix = "(every time a feather script is started)"
    }

    var windowX by windowXP
    var windowY by windowYP
    var windowWidth by windowWidthP
    var windowHeight by windowHeightP
    var windowIsMaximised by windowIsMaximisedP

    var tabSize by tabSizeP
    var clearLog by clearLogP

    init {
        parameters.addAll(
            windowXP, windowYP, windowWidthP, windowHeightP, windowIsMaximisedP,
            tabSizeP, clearLogP
        )
    }

    override fun run() {
        save()
    }

    fun save() {
        saveToPreferences(this, preferences)
    }

    fun load() {
        loadFromPreferences(this, preferences)
    }

    override fun prompt() {
        prompt(false)
    }

    /**
     * We do NOT prompt [instance], instead, we create another instance, and prompt that.
     * This means that while the user changes the values, nothing else gets updated.
     * Then when Save or Apply is pressed, ALL the new values take effect at once.
     */
    override fun prompt(threaded: Boolean) {
        object : Settings() {
            override fun run() {
                super.run()
                Settings.instance.load()
            }
        }.apply {
            load()
            TaskPrompter(this).show()
        }
    }

    companion object {
        val instance = Settings()
    }
}
