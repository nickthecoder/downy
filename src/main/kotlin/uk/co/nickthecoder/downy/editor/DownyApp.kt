package uk.co.nickthecoder.downy.editor

import javafx.application.Application
import javafx.event.EventHandler
import javafx.scene.Scene
import javafx.scene.image.Image
import javafx.scene.media.Media
import javafx.scene.media.MediaPlayer
import javafx.stage.Stage
import javafx.util.Duration
import uk.co.nickthecoder.fxessentials.RecentFiles
import uk.co.nickthecoder.fxessentials.addFxEssentialsCSS
import uk.co.nickthecoder.harbourfx.Harbour
import uk.co.nickthecoder.scarea.Scarea
import java.io.File
import java.util.prefs.Preferences

class DownyApp : Application() {


    override fun start(stage: Stage) {

        Settings.instance.load()
        val gui = GUI()

        if (!gui.harbour.load(preferences())) {
            gui.createDefaultDocks()
        }

        with(stage) {
            val width = Settings.instance.windowWidth ?: 800.0
            val height = Settings.instance.windowHeight ?: 800.0

            scene = Scene(gui, width, height).apply {
                addFxEssentialsCSS()
                Scarea.style(this)
                stylesheets.add(Harbour.compactCssUrl)
                //stylesheets.add(FeditApp::class.java.getResource("fedit.css").toExternalForm())
            }

            if (Settings.instance.windowIsMaximised == true) {
                stage.isMaximized = true
            } else {
                val x = Settings.instance.windowX
                val y = Settings.instance.windowY
                if (x == null || y == null) {
                    stage.centerOnScreen()
                } else {
                    stage.x = x
                    stage.y = y
                }
            }

            title = DEFAULT_TITLE
            icons.add(Image(DownyApp::class.java.getResourceAsStream("feather.png")))
        }

        stage.onHiding = EventHandler {
            with(Settings.instance) {
                windowIsMaximised = stage.isMaximized
                if (!stage.isMaximized) {
                    windowWidth = stage.scene.width
                    windowHeight = stage.scene.height
                }
                windowX = stage.x
                windowY = stage.y
                save()
            }
        }

        for (arg in parameters.unnamed) {
            val file = File(arg)
            if (file.exists()) {
                gui.openFile(file)
            }
        }

        stage.show()

    }

    companion object {

        val DEFAULT_TITLE = "Downy Editor"

        /**
         * Keeps a list of recently opened files (stored in [Preferences]).
         * The [DOCUMENT_OPEN] split menu button uses, and the "recent" menu both use it.
         */
        val recentFiles = RecentFiles(Preferences.userNodeForPackage(DownyApp::class.java).node("recent"))

        private fun preferences(): Preferences = Preferences.userNodeForPackage(DownyApp::class.java)

        private val dingPlayer: MediaPlayer by lazy {
            MediaPlayer(Media(DownyApp::class.java.getResource("ding.wav").toExternalForm()))
        }
        private val errorPlayer: MediaPlayer by lazy {
            MediaPlayer(Media(DownyApp::class.java.getResource("error.wav").toExternalForm()))
        }

        fun playDing() {
            // This line bodges a playback problem. Without it, the 2nd and all subsequent playbacks, there is
            // a "pop" at the end of the sample. Note the sample ends in silence, and plays fine using other software.
            // NOTE, the "error" sound doesn't have this problem.
            dingPlayer.stopTime = dingPlayer.media.duration.subtract(Duration(200.0))

            dingPlayer.stop()
            dingPlayer.play()
        }

        fun playError() {
            errorPlayer.stop()
            errorPlayer.play()
        }

    }

}

fun main(vararg args: String) {
    Application.launch(DownyApp::class.java, * args)
}
