package uk.co.nickthecoder.downy.editor

import javafx.stage.FileChooser
import uk.co.nickthecoder.scarea.Scarea

interface FileType {

    val label: String

    fun attach(scarea: Scarea): Any? = Unit
    fun detach(scarea: Scarea, attachInfo: Any?) {}

    fun attach(gui: GUI): Any? = Unit
    fun detach(gui: GUI, attachInfo: Any?) {}

    fun saveAsDialog(): FileChooser

    /**
     * A graphic for labels, such as tab titles.
     */
    fun graphic() = null

    /**
     * Scores how applicable this file type is to the file used by the tab.
     * 10 if the filetype matches perfectly.
     * 0 if the filetype doesn't match at all.
     */
    fun score(tab: EditorTab): Int = 0
}

class UnknownFileType private constructor() : FileType {

    override val label: String
        get() = "Unknown"

    override fun saveAsDialog() = FileChooser().apply {
        title = "Save"
        extensionFilters.addAll(
            FileChooser.ExtensionFilter("All Files", "*")
        )
    }


    companion object {
        val instance = UnknownFileType()
    }
}


class PlainTextFileType private constructor() : FileType {

    override val label: String
        get() = "Plain Text"

    override fun saveAsDialog() = FileChooser().apply {
        title = "Save"
        extensionFilters.addAll(
            FileChooser.ExtensionFilter("All Files", "*")
        )
    }


    companion object {
        val instance = PlainTextFileType()
    }
}


val fileTypes = listOf<FileType>(
    PlainTextFileType.instance,
    FeatherFileType.instance
)
