package uk.co.nickthecoder.downy.editor

import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.control.CheckMenuItem
import javafx.scene.control.ContextMenu
import javafx.scene.control.MenuItem
import javafx.scene.control.TextArea
import uk.co.nickthecoder.harbourfx.AbstractDockable
import java.io.PrintWriter
import java.io.StringWriter


interface Logger {
    fun println(str: String)
    fun println(obj: Any) = println(obj.toString())

    fun reportError(e: Exception) {
        // Feather is not a dependency of the "core" module so we can't use "is" keyword.
        if (e.javaClass.simpleName == "FeatherException") {
            println(e.toString())
        } else {
            val sw = StringWriter()
            e.printStackTrace(PrintWriter(sw))
            println(sw.toString())
        }
    }
}

class Log {
    companion object {
        @JvmStatic
        var logger: Logger? = null

        @JvmStatic
        fun println(str: String) {
            logger?.println(str)
        }

        @JvmStatic
        fun reportError(e: Exception) {
            logger?.reportError(e)
        }
    }
}


class LogDockable :
    AbstractDockable("log"), Logger {

    private val textArea = TextArea().apply {
        isEditable = false
    }

    override val dockablePrefix = SimpleStringProperty("2 ")

    private val autoScrollProperty = SimpleBooleanProperty(true)

    override val dockableContent: ObjectProperty<Node> = SimpleObjectProperty(textArea)

    init {
        dockableTitle.value = "Log"
        Log.logger = this
    }

    fun clear() {
        textArea.clear()
    }

    override fun settingsMenu() = ContextMenu().apply {
        items.add(MenuItem("Clear").apply {
            onAction = EventHandler {
                textArea.clear()
            }
        })
        items.add(CheckMenuItem("Auto Scroll").apply {
            selectedProperty().bindBidirectional(autoScrollProperty)
            onAction = EventHandler {
                autoScrollProperty.value = !autoScrollProperty.value
            }
        })
    }


    override fun println(str: String) {
        textArea.appendText(str)
        textArea.appendText("\n")
        if (autoScrollProperty.value) {
            textArea.positionCaret(textArea.length)
        }
    }

    companion object {
        val id = "log"
    }
}
